import Application from '@ember/application';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

const App = Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver,

});


App.NotFoundObjectRoute = Ember.Route.extend({
    actions: {
        error: function(reason, transition) {
            if (reason.status == 404)
                return true
            else
                this.transitionTo('r500');
        },
    },
});

loadInitializers(App, config.modulePrefix);

export default App;
