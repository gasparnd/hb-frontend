import Component from '@ember/component';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { equal, notEmpty, or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import currentUser from 'help-button/utils/current-user';
import { htmlSafe } from '@ember/string';


const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  mediaQueries: service(),
  store: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  notifications: service('notification-messages'),
  session: service('session'),
  currentUser: readOnly('session.currentUser'),

  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
    if(!isEmpty(get(this, 'button.buttonNetId'))){
      let d = get(this, 'button.buttonNetId')[0];
      get(this, 'store').query('buttonNet', { 'id': d }).then((buttonNet) => {
        set(this, 'buttonNetImgs',buttonNet);
      });
    }
  },

  imageFile: false,

  showDelete: computed(
    'currentUser.admin',
    'currentUser.superAdmin',
    'button.isMine',
    function() {
      return get(this, 'currentUser.admin') || get(this, 'button.isMine') || get(this, 'currentUser.superAdmin');
    }
  ),

  showEdit: computed(
    'currentUser.admin',
    'currentUser.superAdmin',
    'button.isMine',
    'button.isEditable',
    function() {
      return get(this, 'currentUser.admin') || get(this, 'button.isMine') || get(this, 'currentUser.superAdmin') || get(this, 'button.isEditable');
    }
  ),

  showShare: computed(
    'currentUser.admin',
    'currentUser.superAdmin',
    'button.isMine',
    'button.isEditable',
    function() {
      return true;
    }
  ),

  showTransferField:false,
  showTransfer: computed(
    'currentUser.admin',
    'button.isMine',
    function() {

      const b= get(this, 'currentUser.admin');
       return this.button.isMine;
    }
  ),

  notMineButLog: computed(
    'session.isAuthenticated',
    'button.isMine',
    function() {
      return this.button.isMine;
    }
  ),

  allowEdit: true,
  hideShare: true,
  button: null,
  creator: null,
  creatorAvatar: null,
  isList: null,
  isButtonFile: null,
  isActivateRoute: null,
  email:null,
  isProfile: false,

  allowMoveMap: computed('button', function() {
    return !isEmpty(this.button.toLatitude) || !isEmpty(this.button.toLatitude);
  }),

  hasLocationName: notEmpty('button.locationName'),

  hasFromToLocation: computed(
    'button.latitude',
    'button.longitude',
    'button.toLatitude',
    'button.toLongitude',
    function() {
      return !isEmpty(this.button.latitude) && !isEmpty(this.button.longitude) &&
        !isEmpty(this.button.toLatitude) && !isEmpty(this.button.toLongitude);
    }),

  hasBounds: notEmpty('mapBounds'),

  hasPhone: computed('button.creator', function() {
    const buttonPhone = get(this, 'button.creator.phone');
    const creator = get(this, 'button.creator');
    if (!isEmpty(buttonPhone))
    return buttonPhone;

    return 'telefono';
  }),
  firstMarker: computed(
  'button',
  function() {
    return L.latLng(this.button.latitude, this.button.longitude);
  }),

  secondMarker: computed(
  'button',
   function() {

    return L.latLng(this.button.location.toLatitude, this.button.location.toLongitude);
  }),

  mapBounds: computed('firstMarker', 'secondMarker', function() {

    if (!isEmpty(this.secondmarker))
    return L.latLngBounds(get(this, 'firstMarker'), get(this, 'secondMarker'));

    return L.latLngBounds(get(this, 'firstMarker'),get(this, 'firstMarker'));
  }),

  formatedDate: computed('button.date','button.periodicDate', function() {
    if (isEmpty(get(this, 'button.periodicDate')) || get(this, 'button.periodicDate') === "{}") {
      let date = get(this, 'button.date');
      const options = {
        weekday: 'long', month: 'long',
        day: 'numeric', hour: '2-digit', minute: '2-digit',
      };
      if (!isEmpty(date) && date !== 'Ahora') {
        date = new Date(date);
        date = date.toLocaleTimeString('es-es', options);
        return date.charAt(0).toUpperCase() + date.slice(1);
      }
      return 'Ahora';
    } else {
      let stringDate = get(this, 'button.periodicDate');
      let time = new Date(get(this, 'button.date'));
      const options = {
        hour: '2-digit', minute: '2-digit',
      };
      stringDate = "Los " + stringDate.replace(/,([^,]*)$/, '$1');
      stringDate = stringDate.replace(/,([^,]*)$/, ' y ' + '$1');
      stringDate = stringDate.replace(/,/g, ", ") + " a las " + time.toLocaleTimeString('es-es', options);

      return stringDate;
    }
  }),

  formatedDescription: computed('button.description','currentUser.admin','currentUser.superAdmin',
  function() {
    const b= get(this, 'currentUser.admin');
    const c= get(this, 'currentUser.superAdmin');
    const d= get(this, 'replaceURLWithHTMLLinks');
      if(b || c)
      {
        return d;
      }
      else
      {
        if(d!=null)
        return d.split("##")[0];

        return d;
      }
    }),

  formatedDescriptionResume: computed('button.description','currentUser.admin','this.replaceURLWithHTMLLinks',
  function() {
      const b = get(this, 'currentUser.admin');
      const c = get(this, 'currentUser.superAdmin');

        if(b || c)
        {
          return get(this, 'button.description').substring(0,35)+"...";
        }
        else if(get(this, 'button.description')!=null)
        {
          return get(this, 'button.description').split("##")[0].substring(0,35)+"...";
        } else {
          return "";
        }
      }),

  hasFormatedDate: notEmpty('formatedDate'),

  hasCreatorAvatar: notEmpty('creatorAvatar.content'),

  hasImage: notEmpty('image'),

  image: computed(
    'button',
    {
      get() {
        const button = this.button;
        get(button, 'image').then((image) => {
          set(this, 'image', image);
        });
      },
      set(key, value) {
        return value;
      },
    }
  ),

  cardBtnType: computed(
    'button',
    function() {
      if (!this.button.active && !this.isButtonFile && !this.isActivateRoute) {
        if (get(this, 'button.isChangeButton')) {
          if (this.button.swap) {
            return '--intercambio-swap  card-btn--intercambio-swap-deactivated';
          }
          return '--intercambio card-btn--intercambio-deactivated';
        }
        if (get(this, 'button.isOfferButton')) {
          return '--green card-btn--green-deactivated';
        }
        if (get(this, 'button.isNeedButton')) {
          return '--red card-btn--red-deactivated';
        }
        return null;
      }
      if (get(this, 'button.isChangeButton')) {
        if (this.button.swap) {
          return '--intercambio-swap';
        }
        return '--intercambio';
      }
      if (get(this, 'button.isOfferButton')) {
        return '--green';
      }
      if (get(this, 'button.isNeedButton')) {
        return '--red';
      }
      return null;
    }
  ),

  cardStateBtnType: computed(
    'button',
    function() {
      if (get(this, 'button.isChangeButton')) {
        if (this.button.swap) {
          return '--intercambio-swap';
        }
        return '--intercambio';
      }
      if (get(this, 'button.isOfferButton')) {
        return '--ofrece';
      }
      if (get(this, 'button.isNeedButton')) {
        return '--busca';
      }
      return null;
    }
  ),

  buttonType: computed(
    'button',
    'button.active',
    function() {
      if (!this.button.active && !this.isButtonFile && !this.isActivateRoute) {
        if (get(this, 'button.isChangeButton')) {
          if (this.button.swap) {
            return '--intercambio-swap  card-button--intercambio-swap-deactivated';
          }
          return '--intercambio card-button--intercambio-deactivated';
        }
        if (get(this, 'button.isOfferButton')) {
          return '--ofrece card-button--ofrece-deactivated';
        }
        if (get(this, 'button.isNeedButton')) {
          return '--busca card-button--busca-deactivated';
        }
        return null;
      }
      if (get(this, 'button.isChangeButton')) {
        if (this.button.swap) {
          return '--intercambio-swap';
        }
        return '--intercambio';
      }
      if (get(this, 'button.isOfferButton')) {
        return '--ofrece';
      }
      if (get(this, 'button.isNeedButton')) {
        return '--busca';
      }
      return null;
    }
  ),

  buttonIntention: computed('buttonType', function() {
    const buttonType = this.buttonType;
    switch (buttonType) {
      case '--intercambio':
        return 'OFRECE y BUSCA';
      case '--intercambio-swap':
        return 'BUSCA y OFRECE';
      case '--ofrece':
        return 'OFRECE';
      case '--busca':
        return 'BUSCA / NECESITA';
      case '--intercambio card-button--intercambio-deactivated':
        return 'OFRECE y BUSCA';
      case '--ofrece card-button--ofrece-deactivated':
        return 'OFRECE';
      case '--busca card-button--busca-deactivated':
        return 'BUSCA / NECESITA';
      case '--intercambio-swap  card-button--intercambio-swap-deactivated':
        return 'BUSCA y OFRECE';
      default:
        return 'OFRECE y BUSCA';
    }
  }),

  needsAndOffer: equal('buttonIntention', 'OFRECE y BUSCA'),

  profileRoute: computed(
    'button.isMine',
    function() {
      if (get(this, 'isButtonNetRoute')) {
        return get(this, 'button.isMine') ? 'button-net.profile.index' : 'button-net.user.buttons';
      } else {
        return get(this, 'button.isMine') ? 'profile.index' : 'user.buttons';
      }
    }
  ),

  replaceURLWithHTMLLinks: computed(
    'button.description',
     function() {
      var b = get(this, 'button.description');
      var exp = /(\b(https?|ftp|file):\/\/[\-A-Z0-9+&@#\/%?=~_|!:,.;]*[\-A-Z09+&@#\/%=~_|])/img;
      if(b!=null) {
        b=b.replace(exp,"<a target='_blank' href='$1'>$1</a>");

        // var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
          exp = /(^|[^\/])(www\.[\S]+(\b|$))/img;
          b = b.replace(exp," $1<a href='https://$2' target='_blank'>$2</a>");

         //Change email addresses to mailto:: links.
         var replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
         b = b.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

      }

      return b;
    }
  ),

  linkSegmentId: computed(
    'button.isMine',
    function() {
      const isMine = get(this, 'button.isMine');
      return isMine ? null : this.button.belongsTo('creator').id();
    }
  ),

  hasNetIcon : computed (
    'button.buttonNetId',
    function() {
      return !isEmpty(get(this, 'button.buttonNetId')[0]);
    }
  ),

  buttonNetImgs: null,


  showProfile: null,
  routeType: null,
  removeButtonTransition: null,
  showMap: false,

  actions: {
    toggleAttr(param) {
      this.toggleProperty(param);
    },

    removeButton() {
      this.button.deleteRecord();
      this.button.save()
        .then(() => {
          this.notifications.success(
            'Botón borrado correctamente',
            NOTIFICATION_OPTIONS,
          );
          this.removeButtonTransition();
        })
        .catch(() => {
          this.notifications.error(
            'Error al borrar botón',
            NOTIFICATION_OPTIONS,
          );
        });
    },

    transferButton() {
      const userm=this.get('email');
      get(this, 'store').queryRecord('user', { 'email': userm, 'actionEvent': 'button_transfered', 'button_id': get(this, 'button.id')}).then((user) => {
        set(this, 'button.creator', user);
        set(this, 'button.actionEvent', 'button-transfered');
        this.button.save()
          .then(() => {
            this.notifications.success(
              'Botón transferido correctamente',
              NOTIFICATION_OPTIONS,
            );
          })
          .catch(() => {
            this.notifications.error(
              'Error al transferir botón',
              NOTIFICATION_OPTIONS,
            );
          });
      })
      .catch((error) => {
        if (error.errors[0].title === 'User no encontrado') {
          this.notifications.info(
            'Este email no tiene cuenta, se envió un email de solicitud',
            NOTIFICATION_OPTIONS,
          );
        }
      });
    },

    //WAITING FOR REPORT FUNCTIONALITY
    addReportCount() {
      this.button.deleteRecord();
      this.button.save()
        .then(() => {
          this.notifications.success(
            'Botón reportado',
            NOTIFICATION_OPTIONS,
          );
          this.removeButtonTransition();
        })
        .catch(() => {
          this.notifications.error(
            'Botón reportado',
            NOTIFICATION_OPTIONS,
          );
        });
    },
    onMapLoad(leaflet) {
      L.Routing.control({
        waypoints: [
          L.latLng(this.button.latitude, this.button.longitude),
          L.latLng(this.button.toLatitude, this.button.toLongitude)
        ],
        createMarker: function(i, waypoints, n) {
          return L.marker(L.latLng(0, 0), {
            draggable: false,
          });
	      },
        routeWhileDragging: false,
        addWaypoints: false,
        altLineOptions: true,
      }).addTo(leaflet.target);
    },
    removeFromNet() {
      set(this, 'button.actionEvent', 'button-remove-net');
      get(this, 'button.buttonNetId').splice(get(this, 'button.buttonNetId').indexOf(get(this, 'button.isEditableId')), 1);
      get(this, 'button').save().then(() => {
        this.notifications.success(
          'Botón movido',
          NOTIFICATION_OPTIONS,
        );
        this.removeButtonTransition();
      })
    },
  },
});
