import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { get, set, computed } from '@ember/object';
import { A } from '@ember/array';
import { readOnly } from '@ember/object/computed';
import { isEmpty } from '@ember/utils'
import { setProperties } from '@ember/object'

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  classNames: 'home-welcome-mobile',

  session: service('session'),
  routing: service('-routing'),
  store: service(),
  notifications: service('notification-messages'),


  isShowingMenuModal: null,
  isShowingMenuTags: null,
  isShowingCreateNetModal: false,
  searchTrendingTags: null,
  isSearching: false,
  loading: null,
  togglePropertyMenu: {},
  hideWelcome: false,

  searchZoneAddress:null,
  buttonNet: null,

  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },


  welcomeTitle: computed('titlesArrays', function() {
    return get(this, 'titlesArrays')[0];
  }),

  welcomeTitle1: computed('titlesArrays', function() {
    return get(this, 'titlesArrays')[1];
  }),

  titlesArrays: A([
    'Colabora en temas cerca de: ',
    'Colabora en temas cerca de: ',
    //'Crea un botón de ayuda',
    // 'De todo salimos juntos.',
    // '"Madre mía cómo está la vida"',
    // '"El ser humano… es extraordinario"',
    // '"La herramienta para la vida colaborativa"',
    // '"La herramienta para la vida colaborativa"',
  ]),

  moreTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length > 5;
  }),

  noTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length == 0;
  }),

  firstTrendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];

    if (!isEmpty(tags)) {
      for (let i = 0; i < tags.length; i++) {
        tagsArr.push({name: get(tags[i], 'name'), activeButtonsCounter: get(tags[i], 'activeButtonsCounter')});
      }
      return tagsArr.slice(0,5);
    }
    return tagsArr;
  }),

  firstTrendingTagsObject: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];

    if (!isEmpty(tags)) {
      return tags.slice(0,5);
    }
    return tagsArr;
  }),

  trendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];
    if (!isEmpty(tags)) {
      for (let i = 0; i < Object.keys(tags).length; i++) {
        tagsArr.push({name: Object.keys(tags)[i], activeButtonsCounter: Object.values(tags)[i]});
      }
    }
    return tagsArr;
  }),

  globalTrendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((globalTrendingTags) => {
            set(this, 'globalTrendingTags', globalTrendingTags.slice(0,5));
          });
        // return null;
      },
      set(key, value) { return value; },
    }
  ),

  contextTransition: null,

  actions: {
    toggleModal(param) {
      this.toggleProperty(param);
    },
    create() {
      if(this.isButtonNetRoute) {
        this.send('toggleModal', 'isShowingModal');
        this.transitionToRoute('button-net.index.index.new-button','new');
      } else {
        this.send('toggleModal', 'isShowingModal');
        this.transitionToRoute('index.index.new-button','new');
      }
    },
    hideMenu () {
      this.toggleProperty('isShowingMenuModal');
      if (!isEmpty(get(this, 'contextTransition'))) {
        get(this, 'contextTransition').abort();
      }
    },
    toggleHideWelcome() {
      set(this,'isShowingCreateNetModal',true);
      set(this,'hideWelcome',true);
    },
    addInterest() {
      if(get(this, 'session.isAuthenticated')) {
        const currentUser = get(this, 'session.currentUser');
          get(currentUser, 'userTags').then((userTags) => {
            let t = get(this, 'firstTrendingTagsObject');
            if(get(this, 'isButtonNetRoute') && !isEmpty(get(this, 'buttonNet').tags)){
              t= get(this, 'buttonNet').tags;
            }
            t.forEach((item, i) => {
              let alreadyExists = false;
              for (let j = 0; j < userTags.length; j++) {
                if (userTags.objectAt(j).name === item.name) {
                  alreadyExists = true
                }
              }
              if (!alreadyExists) {
                let tag = get(this, 'store').createRecord('userTag', {
                  name: item.name,
                  tag: item
                });
                tag.save().then((tagResp) => {
                  get(currentUser,'userTags').pushObject(tagResp);
                  currentUser.save();
                  this.notifications.success(
                    'Añadido #'+tagResp.name+' a tus intereses',
                    NOTIFICATION_OPTIONS,
                  );
                });
              } else {
                this.notifications.error(
                  'Ya existe #'+item.name+' en tus intereses',
                  NOTIFICATION_OPTIONS,
                );
              }
            });
          })
      } else {
        const queryParams = {
          latitude: get(this, 'lastUserLat'),
          longitude: get(this, 'lastUserLng'),
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: true,
        };
        this.notifications.error(
          'Debes tener cuenta para poder suscribirte a los temas',
          NOTIFICATION_OPTIONS,
        );
        // if (this.isButtonRoute) {
        //   this.transitionToRoute('button-net.register', {queryParams} );
        // } else {
        //   this.transitionToRoute('register', {queryParams} );
        // }
      }
    },
    searchByAddress(param) {
      set(this, 'isSearching', true);
      set(this, 'loading', true);
      const address = param.formatted_address;
      return this.searchAction(address, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputAddress': null,
            'loading': false,
          });
          this.send('toggleModal', 'isShowingModal');
          // get(this, 'router').transitionTo('index', { queryParams: { buttonsDisplay:"map", hideWelcome:"true", adr: address } })
        });
    },
  },

});
