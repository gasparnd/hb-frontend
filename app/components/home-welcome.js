import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { setProperties, get, set, computed } from '@ember/object';
import { A } from '@ember/array';
import { htmlSafe } from '@ember/string';
import { readOnly } from '@ember/object/computed';
import { isEmpty } from '@ember/utils'

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};


export default Component.extend({
  classNames: ['home-welcome'],
  session: service('session'),
  routing: service('-routing'),
  store: service(),
  notifications: service('notification-messages'),
  router: service(),
  indexController:null,
  isButtonNetRoute: false,
  searchAction: null,
  isSearching: false,
  isShowingCreateNetModal: false,

  lastUserLat: null,
  lastUserLng: null,

  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  buttonNet: null,

  homeUrl: computed(
    function() {
      const buttonUrl = window.location.href;
      return {
        string: buttonUrl,
        encoded: encodeURIComponent(buttonUrl),
      };
    }
  ),

  didRender(){
    if(this.get('input')){
      this.$('input[type=text]:first').focus();
    }
  },

  welcomeTitle: computed('titlesArrays', function() {
    return get(this, 'titlesArrays')[0];
  }),

  welcomeTitle1: computed('titlesArrays', function() {
    return get(this, 'titlesArrays')[1];
  }),

  searchTrendingTags: null,
  searchZoneAddress: null,


  titlesArrays: A([
    'Mira temas para colaborar en la zona: ',
    'Mira posibles temas para colaborar:',

    // '"Crea un botón"',
    // '"El ser humano… es extraordinario"',
    // '"La herramienta para la vida colaborativa"',
    // '"La herramienta para la vida colaborativa"',
  ]),

  moreTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length > 5;
  }),

  noTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length == 0;
  }),

  firstTrendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];

    if (!isEmpty(tags)) {
      for (let i = 0; i < tags.length; i++) {
        tagsArr.push({name: get(tags[i], 'name'), activeButtonsCounter: get(tags[i], 'activeButtonsCounter')});
      }
      return tagsArr.slice(0,5);
    }
    return tagsArr;
  }),

  firstTrendingTagsObject: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];

    if (!isEmpty(tags)) {
      return tags.slice(0,5);
    }
    return tagsArr;
  }),

  trendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];
    if (!isEmpty(tags)) {
      for (let i = 0; i < Object.keys(tags).length; i++) {
        tagsArr.push({name: Object.keys(tags)[i], activeButtonsCounter: Object.values(tags)[i]});
      }
    }
    return tagsArr;
  }),

  globalTrendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((globalTrendingTags) => {
            set(this, 'globalTrendingTags', globalTrendingTags.slice(0,5));
          });
      },
      set(key, value) { return value; },
    }
  ),

  actions: {
    toggleModal(param) {
      this.toggleProperty(param);
    },

    toggleHideWelcome(param) {
      set(this,'isShowingCreateNetModal', true);
      set(this,'hideWelcome',true);
    },

    create() {
      if(this.isButtonNetRoute) {
        this.transitionToRoute('button-net.index.index.new-button','new');
      } else {
        this.transitionToRoute('index.index.new-button','new');
      }
    },
    hideMenu () {
      this.toggleProperty('isShowingMenuModal');
      if (!isEmpty(get(this, 'contextTransition'))) {
        get(this, 'contextTransition').abort();
      }
    },
    copyWidgetRoute() {
      const temporalInput = document.createElement('textarea');
      temporalInput.value = '<iframe src="'+this.homeUrl.string+'" height="600" width="100%" title="Iframe Example"></iframe>';
      temporalInput.setAttribute('readonly', '');
      temporalInput.style = {position: 'absolute', left: '-9999px'};
      document.body.appendChild(temporalInput);

      if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
        // save current contentEditable/readOnly status
        const editable = temporalInput.contentEditable;
        const readOnly = temporalInput.readOnly;

        // convert to editable with readonly to stop iOS keyboard opening
        temporalInput.contentEditable = true;
        temporalInput.readOnly = true;

        // create a selectable range
        const range = document.createRange();
        range.selectNodeContents(temporalInput);

        // select the range
        const selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        temporalInput.setSelectionRange(0, 999999);

        // restore contentEditable/readOnly to original state
        temporalInput.contentEditable = editable;
        temporalInput.readOnly = readOnly;
      } else {
        temporalInput.select();
      }

      if(document.execCommand('copy')) {
        this.notifications.success(
          'Has copiado el código para insertar este mapa en otra web',
          NOTIFICATION_OPTIONS,
        );

      } else {
        this.notifications.error(
          'No se pudo copiar el enlace al mapa',
          NOTIFICATION_OPTIONS,
        );
      }
      document.body.removeChild(temporalInput);
    },
    addInterest() {
      if(get(this, 'session.isAuthenticated')) {
        const currentUser = get(this, 'session.currentUser');
          get(currentUser, 'userTags').then((userTags) => {
            let t = get(this, 'firstTrendingTagsObject');
            if(get(this, 'isButtonNetRoute') && !isEmpty(get(this, 'buttonNet').tags)){
              t= get(this, 'buttonNet').tags;
            }
            t.forEach((item, i) => {
              let alreadyExists = false;
              for (let j = 0; j < userTags.length; j++) {
                if (userTags.objectAt(j).name === item.name) {
                  alreadyExists = true
                }
              }
              if (!alreadyExists) {
                  let tag = get(this, 'store').createRecord('userTag', {
                  name: item.name,
                  tag: item
                });
                tag.save().then((tagResp) => {
                  get(currentUser,'userTags').pushObject(tagResp);
                  currentUser.save();
                  this.notifications.success(
                    'Añadido #'+tagResp.name+' a tus intereses',
                    NOTIFICATION_OPTIONS,
                  );
                });
              } else {
                this.notifications.error(
                  'Ya existe #'+item.name+' en tus intereses',
                  NOTIFICATION_OPTIONS,
                );
              }
            });
          })
      } else {
        const queryParams = {
          latitude: get(this, 'lastUserLat'),
          longitude: get(this, 'lastUserLng'),
          address: null,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: true,
        };
        this.notifications.error(
          'Regístrate para poder suscribirte a los temas',
          NOTIFICATION_OPTIONS,
        );
        // if (this.isButtonRoute) {
        //   this.transitionToRoute('button-net.register', {queryParams} );
        // } else {
        //   this.transitionToRoute('register', {queryParams} );
        // }
      }
    },
    searchByAddress(param) {
      set(this, 'isSearching', true);
      const address = param.formatted_address;
      return this.searchAction(address, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputAddress': null,
          });
        });
    },
  },

});
