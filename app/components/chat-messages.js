import Component from '@ember/component';
import { set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';

export default Component.extend({
  classNames: ['chat-messages col-12'],
  scrollBottom: null,
  messages: null,
  otherUser: null,
  currentUser: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  otherUserAvatar: computed(
    'otherUser',
    {
      get() {
        if (!isEmpty(this.otherUser)) {
          this.otherUser.avatar.then((avatar) => {
            set(this, 'otherUserAvatar', avatar);
          });
        }
      },
      set(key, value) {
        return value;
      },
    }
  ),
});
