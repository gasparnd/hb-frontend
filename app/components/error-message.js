import Component from '@ember/component';

export default Component.extend({
  classNames: ['container-fluid', 'error-message'],

  title: null,
  subtitle: null,
});
