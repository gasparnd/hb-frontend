import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { readOnly } from '@ember/object/computed';
import { or, notEmpty } from '@ember/object/computed';
import { get, set, setProperties, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { A } from '@ember/array';
import { filter } from '@ember/object/computed';


const sortOptions = ['Últimos creados', 'Primeros creados'];

const sortOptionsMine = ['Últimos creados', 'Primeros creados', 'No leídos'];

export default Component.extend({
  store: service(),
  classNames: ['container-fluid', 'buttons-filters'],

  routing: service('-routing'),
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

  profileRoute: false,
  zoomMax: null,
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);

    set(this,'selectedSortMine',"No leídos");
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  userMarkerClass: computed('zoomMax', 'userMarker', function() {
    return get(this, 'userMarker') ?
      'btn btn-link search-map__localize-user search-map__localize-user--selected' :
      'btn btn-link search-map__localize-user';
  }),

  isMapRoute: computed('routing.currentRouteName', function() {
    const currentRouteName = get(this, 'routing.currentRouteName');
    if (currentRouteName === 'index.index') {
      return true;
    }
    return false;
  }),

  isProfileList: computed('routing.currentRouteName', function() {
    const currentRouteName = get(this, 'routing.currentRouteName');
    if (currentRouteName === 'profile.index') {
      return true;
    }
    return false;
  }),

  moreTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length > 5;
  }),

  noTrendingTags: computed('trendingTags', function() {
    return get(this, 'trendingTags').length == 0;
  }),

  firstTrendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];

    if (!isEmpty(tags)) {
      for (let i = 0; i < tags.length; i++) {
        tagsArr.push({name: get(tags[i], 'name'), activeButtonsCounter: get(tags[i], 'activeButtonsCounter')});
      }
      return tagsArr.slice(0,5);
    }
    return tagsArr;
  }),

  // trendingTags: computed('searchTrendingTags', function() {
  //   const tags = get(this, 'searchTrendingTags');
  //   let tagsArr = [];
  //   if (!isEmpty(tags)) {
  //     for (let i = 0; i < Object.keys(tags).length; i++) {
  //       tagsArr.push({name: Object.keys(tags)[i], activeButtonsCounter: Object.values(tags)[i]});
  //     }
  //   }
  //   return tagsArr;
  // }),
  trendingTags: computed('searchTrendingTags', function() {
    const tags = get(this, 'searchTrendingTags');
    let tagsArr = [];
    if (!isEmpty(tags)) {
      for (let i = 0; i < tags.length && i < 15; i++) {
        tagsArr.push({name: get(tags[i], 'name'), activeButtonsCounter: get(tags[i], 'activeButtonsCounter')});
      }
    }
    return tagsArr;
  }),

  isIndexRoute: null,
  resetFilters: null,

  // To apply filters on map move
  didUpdateAttrs() {
    if (!this.isShowingModal && this.buttons !== undefined) {
      !this.lockFilters ? set(this, 'lockFilters', true) : set(this, 'lockFilters', false);
    }
  },

  didRender() {
    if (this.resetFilters) {
      setProperties(this,
        {
          needFilter: false,
          offerFilter: false,
          resetFilters: false,
        });
    }
    if (!this.lockFilters && this.buttons !== undefined) {
      this.send('applyFilters');
    }

    const vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  },

  makeTimestamp(dateString) {
    let arr = dateString.split(/[- :]/);
    let date = new Date(arr[2], arr[1], arr[3], arr[4], arr[5]);
    date.setTime( date.getTime() - date.getTimezoneOffset() * 60 * 1000 );
    return date.getTime();
  },

  lockFilters: true,

  sortOptions: sortOptions,
  sortOptionsMine: sortOptionsMine,
  selectedSort: sortOptions.get('firstObject'),
  selectedSortMine: sortOptionsMine.get('firstObject'),

  buttons: null,

  needFilter: false,
  offerFilter: false,


  unreadFilter: false,
  dateFilter: false,


  isSearching: false,
  searchAction: null,
  inputAddress: null,
  destinationAddress: null,
  inputDate: null,


  selected: computed('this.inputDate', function() {
    return !isEmpty(this.inputDate) ? this.inputDate : new Date();
  }),

  date: computed('this.inputDate', function() {
    return !isEmpty(this.inputDate) ? this.inputDate : new Date();
  }),

  inputFormatedDate: computed('this.inputDate', function() {
    let s = this.inputDate.toString();
    let arr = s.split(/[- :]/);
    let date = arr[2] +'/'+ arr[1] +'/'+ arr[3] +' a las ' +arr[4] +':'+ arr[5];
    // date.setTime( date.getTime() - date.getTimezoneOffset() * 60 * 1000 );
    return date.toString();
  }),

  isShowingModal: null,

  buttonsFiltered: null,

  /**
   * received param to hide the icon link to go to the search page
   * @type {Boolean}
   */
  hideMapLink: false,

  showAddressSearch: null,
  showDestinationSearch: false,
  showDatePicker:false,

  searchedTags: A([]),
  searchTrendingTags: null,
  selectedFilterTags: A([]),
  reduceZoom: null,

  hasSelectedTags: notEmpty('selectedTags'),

  updateSelectedTagsAction() {},
  toggleSelectedTagAction() {},
  localizeUser() {},

  actions: {
    searchAndFilter() {
      set(this, 'isSearching', true);
      if (!isEmpty(this.inputAddress)) {
        this.send('searchByAddress');
      }
      if (!isEmpty(this.destinationAddress)) {
        this.send('searchByDestination');
      }
      this.send('applyFilters');
      set(this, 'isSearching', false);
    },

    applyFilters(varName, selectOption) {
      if (!isEmpty(varName)) {
        (!isEmpty(selectOption) && typeof selectOption === 'string') ?
          set(this, varName, selectOption) : this.toggleProperty(varName);
      }
      const needFilter = this.needFilter;
      const offerFilter = this.offerFilter;
      const unreadFilter = this.unreadFilter;
      const dateFilter = this.dateFilter;

      const buttons = this.buttons;
      this.updateSelectedTagsAction(this.selectedTags);
      let filteredButtons = null;

      // FILTER BY TYPE
      if (needFilter && offerFilter) {
        filteredButtons = buttons.filterBy('buttonType', 'change');
      } else if (needFilter) {
        filteredButtons = buttons.filterBy('buttonType', 'need').concat(buttons.filterBy('buttonType', 'change'));
      } else if (offerFilter) {
        filteredButtons = buttons.filterBy('buttonType', 'offer').concat(buttons.filterBy('buttonType', 'change'));
      } else {
        filteredButtons = buttons;
      }

      // FILTER BY DATE
      if (this.inputDate) {
        var logDate = this.inputDate;
        filteredButtons = buttons.filter(function(button) {
            return  new Date(button.date) >= logDate;
          });

          filteredButtons = filteredButtons.concat(buttons.filter(function(button) {
            return button.get('periodicDate') !== null;
          }));
      }


      // FILTER BY UNREAD messages
      if (get(this, 'isProfileList')) {
        if (get(this, 'selectedSortMine') === 'No leídos') {
          filteredButtons = buttons.sortBy('hasNotifications').reverse();
        } else {
          filteredButtons = filteredButtons.sortBy('createdAt');
          if (get(this, 'selectedSortMine') === 'Últimos creados') {
            filteredButtons = filteredButtons.reverse();
          }
        }
      } else {
        filteredButtons = filteredButtons.sortBy('createdAt');
        if (this.selectedSort === 'Últimos creados') {
          filteredButtons = filteredButtons.reverse();
        }
      }

      set(this, 'buttonsFiltered', filteredButtons);

      if (this.isShowingModal) {
        this.send('toggleModal');
      }
    },



    cleanFilters() {
      setProperties(this, {
        'inputAddress': null,
        'destinationAddress': null,
        'selectedTags': null,
        'needFilter': false,
        'offerFilter': false,
        'inputDate': null,
        'selectedSort': sortOptions.get('firstObject'),
      });
    },



    toggleModal() {
      this.toggleProperty('isShowingModal');
    },
    toggleProperty(property) {
      this.toggleProperty(property);
    },
    searchByAddress() {
      const address = this.inputAddress;
      return this.searchAction(address)
        .then(() => {
          setProperties(this, {
            'isShowingModal': false,
            'inputAddress': null,
          });
        });
    },

    searchByDate() {
      const date = this.inputDate;
      return this.searchAction(date)
        .then(() => {
          setProperties(this, {
            'isShowingModal': false,
            'inputDate': null,
          });
        });
    },

    searchByDestination() {
      const address = this.destinationAddress;
      return this.searchAction(address)
        .then(() => {
          setProperties(this, {
            'isShowingModal': false,
            'destinationAddress': null,
          });
        });
    },

    searchTags(term, selectClass) {
      if (term.includes(' ')) {
        selectClass.actions.close();
        const fixedTerm = term.replace(' ', '');
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            return get(this, 'selectedTags').pushObject(tags.get('firstObject'));
          });
      }
      return get(this, 'store').query('tag', { 'filter[name]': term })
        .then((tags) => {
          set(this, 'tags', tags);
          return tags;
        });
    },

    cleanDate() {
      setProperties(this, {
        'inputDate': null,
      });
      this.send('closeDateModal');
    },

    confirmDate() {
      const date = this.selected;
      // set(this, 'isSearching', true);
      date.setHours(this.date.getHours());
      date.setMinutes(this.date.getMinutes());
      set(this, 'inputDate', date);
      // this.send('applyFilters');
      this.send('closeDateModal');
      // set(this, 'isSearching', false);
    },

    changeSelectedTags(tags) {
      set(this, 'selectedTags', tags);
    },



    removeTag(tag) {
      const target = get(this, 'selectedTags');
      target.removeObject(tag);
    },

    tagSuggestion() {
      return '';
    },

    closeDateModal() {
      set(this, 'showDatePicker', false);
    },

    closedToggleSelectedTag([ tag ]) {
      this.toggleSelectedTagAction(tag);
    },

    reduceZoom() {
      this.reduceZoom();
    },

    localizeUser() {
      return this.localizeUser();
    },
  },
});
