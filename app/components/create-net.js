import Component from '@ember/component';
import { get, computed, set } from '@ember/object';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';
import { htmlSafe } from '@ember/string';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};
const NOTIFICATION_OPTIONS2 = {
  autoClear: true,
  clearDuration: 10000,
  cssClasses: 'notifications-index',
};

export default Component.extend({
  classNames: ['container-fluid'],
  classNameBindings: ['create-net'],
  notifications: service('notification-messages'),
  routing: service('-routing'),
  session: service('session'),
  store: service(),
  screens: service('screen'),
  router: service(),
  isButtonNetRoute: false,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },
  hideCreateNet:null,


  didRender() {
    this._super(...arguments);
    set(this, 'didRenderPage', true);
  },

  isShowingCreateNetModal: null,

  svgMaxHeight: computed(
    'screens.width',
    'screens.height',
    'didRenderPage',
    function() {
      this.screens.width;
      this.screens.height;
      let bodyHeight = document.getElementsByClassName('register-component__modal-content')[0];
      let contentHeight = document.getElementById('typeContent');
      if (!isEmpty(bodyHeight) && !isEmpty(contentHeight)) {
        bodyHeight = bodyHeight.clientHeight;
        contentHeight = contentHeight.clientHeight;
        let svgStyles;
        if (this.isModal && this.screens.width < 992) {
          svgStyles = bodyHeight - contentHeight + 54;
        } else if (this.isModal && this.screens.width > 992) {
          svgStyles = bodyHeight - contentHeight - 32;
        } else {
          svgStyles = this.screens.height - contentHeight - 16 - 75;
        }
        if (svgStyles < 100) {
          svgStyles = 100;
        } else if (svgStyles > 350) {
          svgStyles = 300;
        }
        svgStyles = `height: ${svgStyles}px;`;
        return htmlSafe(svgStyles);
      }
      return htmlSafe(`height: ${100}px;`);
    }
  ),

  isModal: null,

  isMobileButtonCreation: null,

  registerComponentBindClass: computed(
    'isModal',
    'isMobileButtonCreation',
    'screens.height',
    'svgMaxHeight',
    function() {
      if (this.isModal) {
        return 'register-component__modal';
      }
      if (this.isMobileButtonCreation) {
        return 'register-component--taller';
      }
      const imageHeight = document.getElementsByClassName('register-component__image')[0];
      if (!isEmpty(imageHeight) && imageHeight.clientHeight > 100) {
        return 'register-component__main-page register-component__main-page--high';
      }
      return 'register-component__main-page';
    }
  ),

  newUser: null,

  modalClasses: computed('isModal', function() {
    return this.isModal ? 'col-10' : 'col-12 col-sm-10 col-md-7 col-lg-9';
  }),

  routeName: null,

  isShowingMoreOptions: false,

  loginTransition: null,

  actions: {
    register() {
      const self=this;
      const newUser = this.newUser;
      const notification = this.notifications;

      return newUser.save()
        .then((user)=>{
          notification.success(
            '¡Enhorabuena, registro completado!',
            NOTIFICATION_OPTIONS
          );
          const email = get(user, 'email');
          const password = get(user, 'randomPassword');

          return this.session
            .authenticate('authenticator:tiddle', email, password)
            .then(() => {
              this.notifications.success(
                'Bienvenid@ a Helpbuttons! Enviamos la contraseña a tu mail. Puedes cambiar tus datos y contraseña en tu perfil',
                NOTIFICATION_OPTIONS2
              );
            })
            .catch((error)=>{
              return error;
            });
        })
        .catch((error)=>{
          switch (error.errors[0].detail) {
            case "Correo ya registrado, logéate o inténtalo con otro correo.":
              const email = get(newUser, 'email');

              this.notifications.error(
                  email+' es un correo ya registrado, añade su contraseña.',
                  NOTIFICATION_OPTIONS
              );
              if (self.get('router')._router.currentPath.includes('index.index.button')) {
                if (get(this, 'isButtonNetRoute')) {
                  self.get('router').transitionTo('button-net.index.index.button.login',{
                    queryParams: {
                      email: email
                    }
                  });
                } else {
                  self.get('router').transitionTo('index.index.button.login',{
                    queryParams: {
                      email: email
                    }
                  });
                }
              } else if (self.get('router')._router.currentPath.includes('index.index.new-button')) {
                if (get(this, 'isButtonNetRoute')) {
                  self.get('router').transitionTo('button-net.index.index.new-button.login',{
                    queryParams: {
                      email: email
                    }
                  });
                } else {
                  self.get('router').transitionTo('index.index.new-button.login',{
                    queryParams: {
                      email: email
                    }
                  });
                }
              } else if (self.get('router')._router.currentPath.includes('profile.new-button')) {
                if (get(this, 'isButtonNetRoute')) {
                  self.get('router').transitionTo('button-net.profile.new-button.login',{
                    queryParams: {
                      email: email
                    }
                  });
                } else {
                  self.get('router').transitionTo('profile.new-button.login',{
                    queryParams: {
                      email: email
                    }
                  });
                }
              } else {
                if (get(this, 'isButtonNetRoute')) {
                  self.get('router').transitionTo('button-net.login',{
                    queryParams: {
                      email: email
                    }
                  });
                } else {
                  self.get('router').transitionTo('login',{
                    queryParams: {
                      email: email
                    }
                  });
                }
              }


              break;
            default:
              this.notifications.error(
                  error.errors[0].detail,
                  NOTIFICATION_OPTIONS
                );
          }
          return error;
        });
    },


    onClick() {
      this.onCreate();
      this.select.actions.close();
    },

    toggleShowMoreOptions() {
      this.toggleProperty('isShowingCreateNetModal');
    },

    createNet() {
      if(get(this, 'session.isAuthenticated')) {
        this.selectRemoteControl.actions.close();
        this.toggleProperty('isShowingCreateNetModal');
      } else {
        if (get(this, 'isButtonNetRoute')) {
          this.notifications.warning(
              'Debes registrarte primero',
              NOTIFICATION_OPTIONS
          );
          this.get('router').transitionTo('button-net.register');
        } else {
          this.notifications.warning(
              'Debes registrarte primero',
              NOTIFICATION_OPTIONS
          );
          this.get('router').transitionTo('register');
        }
      }
    },

    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index') {
          context.loginTransition();
        }
      }, context);
    },
  },
});
