import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { or, readOnly, alias } from '@ember/object/computed';
import { computed, set, get, setProperties } from '@ember/object';
import { A } from '@ember/array';
import { isEmpty } from '@ember/utils';


export default Component.extend({
  classNames: ['component-header', 'd-block'],
  classNameBindings: ['useContainer'],
  store: service(),
  router: service(),
  isButtonNetRoute: false,
  loading: null,
  init() {
    this._super(...arguments);
    set(this, 'isButtonNetRoute', this.router._router.currentPath.includes('button-net'));
  },

  session: service('session'),
  ajax: service(),
  mediaQueries: service(),
  routing: service('-routing'),

  buttonNet: null,

  isAuthenticated: readOnly('session.isAuthenticated'),

  currentUser: alias('session.currentUser'),

  currentUserAvatar: readOnly('session.currentUser.avatar'),


  searchTrendingTags: null,

  modalBackground: computed('isAuthenticated', function() {
    if (this.isAuthenticated) {
      return 'component-header__container-modal';
    }
    return 'p-2 bg-primary h-100 w-100';
  }),

  useContainer: computed('isSmallDevice', function() {
    if (this.isSmallDevice) {
      return '';
    }
    return 'container-fluid';
  }),

  trendingTags: computed(
    {
      get() {
        this.store.query('tag', { 'filter[popular]': true })
          .then((trendingTags) => {
            set(this, 'trendingTags', trendingTags);
          });
        return null;
      },
      set(key, value) { return value; },
    }
  ),

  //FORM PLACEHOLDER FOR PROFILE VS index
  searchPlaceholder: computed ( function() {
      if(this.isMyButtonsRoute)
      {
        return 'Busca en tus botones';
      }
      else
      {
        return 'Buscar por ej: #comida #deporte';
      }
    }
  ),

  showLinkToHome: null,
  showBackBtn: null,

  isMyButtonsRoute: null,
  isIndexRoute: false,
  isUserRoute: false,

  isShowingModal: false,
  isShowingMenuModal: false,
  isShowingCreateNetModal:null,
  showDatePicker:false,
  showDestinationSearch: false,

  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  isModal: !alias('this.isSmallDevice'),

  //DATES AND address
  searchAction: null,
  inputAddress: null,
  inputDate: null,
  fromAddress: null,
  toAddress: null,

  selected: computed('this.inputDate', function() {
    return !isEmpty(this.inputDate) ? this.inputDate : new Date();
  }),

  date: computed('this.inputDate', function() {
    return !isEmpty(this.inputDate) ? this.inputDate : new Date();
  }),

  inputFormatedDate: computed('inputDate', function() {
    let s = this.inputDate.toString();
    let arr = s.split(/[- :]/);
    let date = arr[2] +'/'+ arr[1] +'/'+ arr[3] +' a las ' +arr[4] +':'+ arr[5];
    // date.setTime( date.getTime() - date.getTimezoneOffset() * 60 * 1000 );
    return date.toString();
  }),

  hideWelcome: null,

  loginTransition: null,
  contextTransition: null,

  selectedFilterTags: A([]),

  toggleSelectedTagAction() {},

  actions: {
    searchByAddress(param) {
      set(this, 'isSearching', true);
      const address = param.formatted_address;
      return this.searchAction(address, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputAddress': null,
          });
        });
    },

    confirmDate() {
      const date = this.selected;
      set(this, 'isSearching', true);
      date.setHours(this.date.getHours());
      date.setMinutes(this.date.getMinutes());
      set(this, 'inputDate', date);
      this.send('applyFilters');
      this.send('closeModal');
      set(this, 'isSearching', false);
    },

    searchByDate() {
      set(this, 'isSearching', true);
      const date = this.inputDate;
      return this.searchAction(date, get(this, 'selectedFilterTags'))
        .then(() => {
          setProperties(this, {
            'isSearching': false,
            'inputDate': null,
          });
        });
    },
    toggleModal(param) {
      this.toggleProperty(param);
      if (!isEmpty(get(this, 'contextTransition'))) {
        get(this, 'contextTransition').abort();
      }
    },
    closedToggleSelectedTag([ tag ]) {
      if (this.routing.currentRouteName.includes('index.index') ||
      (this.routing.currentRouteName.includes('profile') &&
      !this.routing.currentRouteName.includes('profile-edition'))) {
        this.toggleSelectedTagAction(tag);
      } else {
        set(this, 'isSearching', true);
        const address = this.inputAddress;
        return this.searchAction(address, tag.name)
          .then(() => {
            setProperties(this, {
              'isSearching': false,
              'inputAddress': null,
            });
          });
      }
    },
    searchTags(term, selectClass) {
      let fixedTerm = term;
      fixedTerm=fixedTerm.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      fixedTerm=fixedTerm.replace(/(~|`|!|@|#|$|%|^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,"");
      if (term.includes(' ')) {
        selectClass.actions.close();
        fixedTerm = term.replace(' ', '');
        return get(this, 'store').query('tag', { 'filter[name]': fixedTerm })
          .then((tags) => {
            if (!isEmpty(tags.get('firstObject'))) {
              return this.toggleSelectedTagAction(tags.get('firstObject'));
            }
            return null;
          });
      }
      return this.store.query('tag', { 'filter[name]': fixedTerm })
        .then((tags) => {
          return tags.filter((tag) => !this.selectedFilterTags.includes(tag));
        });
    },

    tagSuggestion() {
      return '';
    },

    closeDropdown(dropdown) {
      if (!this.routing.currentRouteName.includes('index.index')
      && !this.routing.currentRouteName.includes('profile.index')
      && !this.routing.currentRouteName.includes('user.buttons')
      && !this.routing.currentRouteName.includes('others')
      && !this.routing.currentRouteName.includes('profile-edition')) {
        dropdown.actions.close();
      }
    },
    toggleCreateButtonNet() {
      this.toggleProperty('isShowingCreateNetModal');
    },
    closeModal() {
      set(this, 'showDatePicker', false);
    },
    toggleProperty(property) {
      this.toggleProperty(property);
    },
    rrssAuthenticate(providerName) {
      const context = this;
      this.session.authenticate('authenticator:torii', providerName).then(function() {
        if (context.routing.currentRouteName === 'index.index') {
          context.loginTransition();
        }
      }, context);
    },

  },
});
