import Component from '@ember/component';
import DS from 'ember-data';
import { readOnly, and } from '@ember/object/computed';
import { get, set, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';

const { PromiseObject } = DS;

export default Component.extend({
  tagName: 'button',

  classNames: ['promise-button', 'overflow-text'],

  classNameBindings: [
    'promiseIsPending:promise-button--loading',
    'replaceContentWhenLoading:promise-button--no-text',
  ],

  attributeBindings: ['type', 'disabled', 'title'],

  disabled: computed(
    'promiseObject.isPending',
    'otherDisablingOption',
    function() {
      return get(this, 'promiseObject.isPending') || this.otherDisablingOption;
    }
  ),

  type: 'button',
  /**
   * used for buttons that need a second
   * disabling property like 'accept terms', 'confirm'...
   *
   * @property otherDisablingOption
   * @type {Boolean}
   */
  otherDisablingOption: null,

  /**
   * action passed to the 'promise-button' component
   */
  btnAction: null,

  promiseObject: null,

  promiseIsPending: readOnly('promiseObject.isPending'),
  hideContentOnLoading: false,

  replaceContentWhenLoading: and('promiseIsPending', 'hideContentOnLoading'),

  click() {
    const promise = this.btnAction();
    if(!isEmpty(promise)) {
      const promiseObject = PromiseObject.create({promise});
      set(this, 'promiseObject', promiseObject);
    }
  },
});
