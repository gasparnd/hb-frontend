// <Helpbuttons collaborative network making software>
//     Copyright (C) 2013-2121  Helpbuttons
//
//     This program is free software; you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation; either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License along
//     with this program; if not, write to the Free Software Foundation, Inc.,
//     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
import JSONAPIAdapter from 'ember-data-updating-json-api-relationships/adapters/adapter';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import { inject as service } from '@ember/service';
import config from '../config/environment';

export default JSONAPIAdapter.extend(
  DataAdapterMixin,
  {
    session: service(),
    namespace: config.namespace,

    authorize(xhr) {
      const sessionData = this.get('session.data.authenticated');
      xhr.setRequestHeader('X-USER-EMAIL', `${sessionData.email}`);
      xhr.setRequestHeader('X-USER-TOKEN', `${sessionData.token}`);
    },
  }
);
