import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  headTags: [{
    type: 'meta',
    tagId: 'meta-twitter-card',
    attrs: {
      name: 'twitter:card',
      content: 'summary_large_image',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-site',
    attrs: {
      name: 'twitter:site',
      content: '@HelpButtons',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-creator',
    attrs: {
      name: 'twitter:creator',
      content: '@HelpButtons',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-title',
    attrs: {
      name: 'twitter:title',
      content: 'HelpButtons - Crear nuevo botón',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-description',
    attrs: {
      name: 'twitter:description',
      content: 'Crea botones que otros pueden pulsar, comparte en redes y encuentra personas',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-image',
    attrs: {
      name: 'twitter:image',
      content: 'https://www.dropbox.com/s/jxb5jqtjxvty6m4/imagen_redes_por_defecto.png?dl=0',
    },
  },
  ],

  model(params) {
    if (params.button_id === 'new') {
      return this.store.createRecord('button');
    }
    const button = this.store.peekRecord('button', params.button_id);
    if (isEmpty(button)) {
      return this.store.findRecord('button', params.button_id).then((btn) => {
        if (btn.isMine) {
          return btn;
        }
        return this.transitionTo('profile');
      });
    }
    if (button.isMine) {
      return button;
    }
    return this.transitionTo('profile');
  },

  actions: {
    toggleButtonState() {
      this.toggleProperty('button.active');
      this.button.save();
    },
  },
});
