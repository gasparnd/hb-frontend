import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    const button = this.modelFor('profile.new-button');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('profile.new-button');
    }
  },
});
