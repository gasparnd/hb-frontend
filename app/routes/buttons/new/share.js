import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    const button = this.modelFor('buttons.new');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('buttons.new');
    } else if (!button.hasValidTags || isEmpty(button.description)) {
      this.transitionTo('buttons.new.description');
    } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
      this.transitionTo('buttons.new.location');
    }
  },
});
