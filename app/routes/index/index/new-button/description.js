import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    const button = this.modelFor('index.index.new-button');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('index.index.new-button');
    }
  },
});
