import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { set } from '@ember/object';

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    authenticationRoute: 'index.index.new-button.register',
    session: service(),

    beforeModel() {
      this._super(...arguments);
      const button = this.modelFor('index.index.new-button');
      if (isEmpty(button) || isEmpty(button.buttonType)) {
        this.transitionTo('index.index.new-button');
      } else if (!button.hasValidTags || isEmpty(button.description)) {
        this.transitionTo('index.index.new-button.description');
      } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
        this.transitionTo('index.index.new-button.location');
      }
    },

    model() {
      if (isEmpty(this.session.currentUser)) {
        return this.store.findRecord('user', 'current').then((currentUser) => {
          set(this, 'session.currentUser', currentUser);
          return this.modelFor('index.index.new-button');
        });
      }
      return this.modelFor('index.index.new-button');
    },
  }
);
