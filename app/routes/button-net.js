import Route from '@ember/routing/route';
// import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { set, get, computed } from '@ember/object';
import { resolve } from 'rsvp';
import { isEmpty } from '@ember/utils'


const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Route.extend(
  // UnauthenticatedRouteMixin,
  {
    // authenticationRoute: 'button-net.register',

    authenticationRoute: null,

    session: service(),
    transitionVar: null,
    privacy:null,

    beforeModel(transition) {
      this._super(...arguments);
      set(this, 'transitionVar', transition);
    },


    model(param) {
      if (param['button_net_name'] === 'new') {
        const context = this;
        return setTimeout(function () {
          return context.transitionTo('index.index.new-button.description');
        }, 10);
      }
      const currentUser = this.session.currentUser;
      return this.store.queryRecord('buttonNet', { name: param['button_net_name']}).then((buttonNet) => {
        if (isEmpty(buttonNet)) {
          return get(this, 'transitionVar').abort();
        } else {
          if (!isEmpty(get(this, 'session.currentUser')) && get(this, 'session.isAuthenticated')) {
            if (isEmpty(get(this, 'session.currentUser.internalId'))) {
              get(this, 'session.currentUser').then((currentUser) => {
                if (!isEmpty(get(this, 'session.currentUser.internalId'))) {
                  if (get(buttonNet, 'blockedUsers').includes(get(this, 'session.currentUser.internalId'))) {
                    this.transitionTo('index.index');
                    const newNet = Ember.Object.create({
                              name: 'Todas las redes'
                          });
                    newNet.set('name','Todas las redes');
                    newNet.set('imgUrl','network');
                    newNet.set('id',0);
                    set(this.controllerFor('index'), 'buttonNet', newNet);
                    // Notificacion
                    this.notifications.error(
                      'Has sido bloqueado en esta red',
                      NOTIFICATION_OPTIONS,
                    );
                    return get(this, 'transitionVar').abort();
                  } else if (get(buttonNet, 'privacy') && !get(buttonNet, 'allowedUsers').includes(get(this, 'session.currentUser.internalId'))) {
                    set(this, 'showAllowModal', true);
                  }
                } else if (get(buttonNet, 'privacy')) {
                  // return get(this, 'transitionVar').abort();
                  return this.transitionTo('button-net.register');
                }
              })
            } else {
              if (get(buttonNet, 'blockedUsers').includes(this.session.currentUser.internalId)) {
                this.transitionTo('index.index');
                const newNet = Ember.Object.create({
                          name: 'Todas las redes'
                      });
                newNet.set('name','Todas las redes');
                newNet.set('imgUrl','network');
                newNet.set('id',0);
                set(this.controllerFor('index'), 'buttonNet', newNet);
                // Notificacion
                this.notifications.error(
                  'Has sido bloqueado en esta red',
                  NOTIFICATION_OPTIONS,
                );
                return get(this, 'transitionVar').abort();
              } else if (get(buttonNet, 'privacy') && !get(buttonNet, 'allowedUsers').includes(this.session.currentUser.internalId)) {
                set(this, 'showAllowModal', true);
              }
            }
          } else {
            if (get(buttonNet, 'privacy')) {
              this.transitionTo('button-net.register');
            }
          }
        }
        if (buttonNet.privacy) {
          set(this,'authenticationRoute','button-net.register')
        }
        return buttonNet;
      })
      .catch(err => {
        if (this.transitionVar.router.oldState == undefined) {
          get(this, 'transitionVar').abort();
          return this.transitionTo('error')
        }
        return get(this, 'transitionVar').abort();
      });

    },



    setupController(controller, model) {
      if(model.privacy)
      set(this,'authenticationRoute','button-net.register')

      this._super(controller, model);
      if (!isEmpty(get(this, 'showAllowModal'))) {
        set(controller, 'showAllowModal', get(this, 'showAllowModal'));
      }
    },
  }
);
