import Route from '@ember/routing/route';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import { get, set } from '@ember/object';
import { isEmpty } from '@ember/utils';

export default Route.extend(
  UnauthenticatedRouteMixin,
  {
    headTags: [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: 'HelpButtons - Regístrate',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: 'Registrate ahora para acceder a Helpbuttons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: 'https://www.dropbox.com/s/xni2lreczd587fo/imagen_pordefecto_otros.png?dl=0',
      },
    },
    ],

    modelRoute: 'button-net',
    buttonNetName: null,
    beforeModel(transition) {
      this._super(...arguments);
      set(this, 'buttonNetName', transition.params['button-net'].button_net_name);
    },

    queryParams: {email: {refreshModel: true}},

    model() {
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
      return this.store.createRecord('user');
    },

    setupController(controller, model) {
      this._super(controller, model);
      if (!isEmpty(get(model, 'queryParams.email'))) {
        set(model, 'email', get(model, 'queryParams.email'));
      }
      this.set('queryParams', get(this, 'queryParams'));
      set(controller, 'model', model);
      this.store.queryRecord('buttonNet', { name: get(this, 'buttonNetName')}).then((buttonNet) => {
        set(this, 'controller.buttonNet', buttonNet);
      });
    },
  }
);
