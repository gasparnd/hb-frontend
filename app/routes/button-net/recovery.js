import Route from '@ember/routing/route';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import { get, set } from '@ember/object';

export default Route.extend(
  UnauthenticatedRouteMixin,
  {
    headTags: [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: 'HelpButtons - Recuperar constraseña',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: 'Restaura tu contraseña mediante tu email',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: 'https://www.dropbox.com/s/xni2lreczd587fo/imagen_pordefecto_otros.png?dl=0',
      },
    },
    ],

    buttonNetName: null,
    beforeModel(transition) {
      this._super(...arguments);
      set(this, 'buttonNetName', transition.params['button-net'].button_net_name);
    },
    setupController(controller, model) {
      this._super(controller, model);
      this.store.queryRecord('buttonNet', { name: get(this, 'buttonNetName')}).then((buttonNet) => {
        set(this, 'controller.buttonNet', buttonNet);
      });
    },
    deactivate() {
      set(this.controller, 'errorMessages', null);
    },
  }
);
