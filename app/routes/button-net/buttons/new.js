import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';
import { get, set } from '@ember/object'

export default Route.extend({
  headTags: [{
    type: 'meta',
    tagId: 'meta-twitter-card',
    attrs: {
      name: 'twitter:card',
      content: 'summary_large_image',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-site',
    attrs: {
      name: 'twitter:site',
      content: '@HelpButtons',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-creator',
    attrs: {
      name: 'twitter:creator',
      content: '@HelpButtons',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-title',
    attrs: {
      name: 'twitter:title',
      content: 'HelpButtons - Crear nuevo botón',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-description',
    attrs: {
      name: 'twitter:description',
      content: 'Crea botones que otros pueden pulsar, comparte en redes y encuentra personas',
    },
  },
  {
    type: 'meta',
    tagId: 'meta-twitter-image',
    attrs: {
      name: 'twitter:image',
      content: 'https://www.dropbox.com/s/xni2lreczd587fo/imagen_pordefecto_otros.png?dl=0',
    },
  },
  ],

  dontCreateButton: false,

  beforeModel(transition) {
    this._super(...arguments);
    if (transition.targetName.includes('login') || transition.targetName.includes('register')) {
      set(this, 'dontCreateButton', true);
    } else {
      set(this, 'dontCreateButton', false);
    }
  },

  model(params) {
    if (params.button_id === 'new' && !get(this, 'dontCreateButton')) {
      return this.store.createRecord('button');
    }
    if (get(this, 'dontCreateButton')) {
      return this.store.peekAll('button').filterBy('isNew', true).lastObject;
    }
    const button = this.store.peekRecord('button', params.button_id);
    if (isEmpty(button)) {
      return this.store.findRecord('button', params.button_id).then((btn) => {
        if (btn.isMine) {
          return btn;
        }
        return this.transitionTo('button-net.index.index');
      });
    }
    if (button.isMine) {
      this.toggleProperty('button.active');
      return button;
    }
    return this.transitionTo('button-net.index.index');
  },
});
