import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import { inject as service } from '@ember/service';
import { set } from '@ember/object';

export default Route.extend(
  AuthenticatedRouteMixin,
  {
    authenticationRoute: 'button-net.profile.new-button.register',
    session: service(),

    beforeModel() {
      this._super(...arguments);
      const button = this.modelFor('button-net.profile.new-button');
      if (isEmpty(button) || isEmpty(button.buttonType)) {
        this.transitionTo('button-net.profile.new-button');
      } else if (!button.hasValidTags || isEmpty(button.description)) {
        this.transitionTo('button-net.profile.new-button.description');
      } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
        this.transitionTo('button-net.profile.new-button.location');
      }
    },

    model() {
      if (isEmpty(this.session.currentUser)) {
        return this.store.findRecord('user', 'current').then((currentUser) => {
          set(this, 'session.currentUser', currentUser);
          return this.modelFor('button-net.profile.new-button');
        });
      }
      return this.modelFor('button-net.profile.new-button');
    },
  }
);
