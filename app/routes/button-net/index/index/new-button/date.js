import Route from '@ember/routing/route';
import { isEmpty } from '@ember/utils';

export default Route.extend({
  beforeModel() {
    this._super(...arguments);
    const button = this.modelFor('button-net.index.index.new-button');
    if (isEmpty(button) || isEmpty(button.buttonType)) {
      this.transitionTo('button-net.index.index.new-button');
    } else if (!button.hasValidTags || isEmpty(button.description)) {
      this.transitionTo('button-net.index.index.new-button.description');
    } else if (isEmpty(button.latitude) || isEmpty(button.longitude)) {
      this.transitionTo('button-net.index.index.new-button.location');
    }
  },
});
