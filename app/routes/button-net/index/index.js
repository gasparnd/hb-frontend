import Route from '@ember/routing/route';
import { get, set, setProperties } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';

export const POLL_INTERVAL = 1000;


export default Route.extend({
  geolocation: service('geolocation'),

  headTags() {
    const controller = this.controllerFor(this.routeName);
    return [{
      type: 'meta',
      tagId: 'meta-twitter-card',
      attrs: {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-site',
      attrs: {
        name: 'twitter:site',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-creator',
      attrs: {
        name: 'twitter:creator',
        content: '@HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-title',
      attrs: {
        name: 'twitter:title',
        content: controller.get('buttonsDisplay') === 'list' ? 'HelpButtons - Listado de botones' : 'HelpButtons',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-description',
      attrs: {
        name: 'twitter:description',
        content: 'Helpbuttons es tu herramienta colaborativa personalizable mediante #tags',
      },
    },
    {
      type: 'meta',
      tagId: 'meta-twitter-image',
      attrs: {
        name: 'twitter:image',
        content: 'https://www.dropbox.com/s/xni2lreczd587fo/imagen_pordefecto_otros.png?dl=0',
      },
    },
    ];
  },


  getUserLocation() {
    const geolocation = get(this, 'geolocation');
    return geolocation.getCurrentPosition()
      .then((coordinates) => {
        const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);
        set(this, 'userPosition', userPosition);
        return userPosition;
      });
  },

  onPoll() {
    const indexController = this.controllerFor('button-net.index.index');
    if(indexController.get('userMarker')) {
      const geolocation = get(this, 'geolocation');
      return geolocation.getCurrentPosition()
        .then((coordinates) => {
          const userPosition = L.latLng(coordinates.latitude, coordinates.longitude);
          if (isEmpty(get(indexController, 'lastUserLat')) || isEmpty(get(indexController, 'lastUserLng'))) {
            set(indexController, 'lastUserLat', userPosition.lat);
            set(indexController, 'lastUserLng', userPosition.lng);
            const queryParams = {
              latitude: userPosition.lat,
              longitude: userPosition.lng,
              address: null,
              northEastLat: null,
              northEastLng: null,
              southWestLat: null,
              southWestLng: null,
              userMarker: true,
            };
            this.transitionTo('button-net.index', { queryParams });
          } else {
            set(indexController, 'userPosition', userPosition);
            const maxPosLat = userPosition.lat > get(indexController, 'lastUserLat') ?
              userPosition.lat : get(indexController, 'lastUserLat');
            const minPosLat = userPosition.lat > get(indexController, 'lastUserLat') ?
              get(indexController, 'lastUserLat') : userPosition.lat;

            const maxPosLng = userPosition.lng > get(indexController, 'lastUserLng') ?
              userPosition.lng : get(indexController, 'lastUserLng');
            const minPosLng = userPosition.lng > get(indexController, 'lastUserLng') ?
              get(indexController, 'lastUserLng') : userPosition.lng;
            if ((maxPosLat - minPosLat > 0.0002) || (maxPosLng - minPosLng > 0.0002)) {
              set(indexController, 'lastUserLat', userPosition.lat);
              set(indexController, 'lastUserLng', userPosition.lng);
              const queryParams = {
                latitude: get(indexController, 'lastUserLat'),
                longitude: get(indexController, 'lastUserLng'),
                address: null,
                northEastLat: null,
                northEastLng: null,
                southWestLat: null,
                southWestLng: null,
                userMarker: true,
              };
              this.transitionTo('button-net.index', { queryParams });
            }
          }
        });
    }
    return Promise.resolve();
  },

  afterModel() {
    let usersPoller = this.get('usersPoller');

    // Make sure we only create one poller instance. Without this every time onPoll
    // is called afterModel would create a new poller causing us to have a growing list
    // of pollers all polling the same thing (which would result in more frequent polling).
    if (!usersPoller) {
      usersPoller = this.get('pollboy').add(this, this.onPoll, POLL_INTERVAL);
      this.set('usersPoller', usersPoller);
    }
  },

  deactivate() {
    const usersPoller = this.get('usersPoller');
    this.get('pollboy').remove(usersPoller);
    set(this.controllerFor('button-net.index.index'), 'loadedMap', false);
  },

  centerLeafletMapOnMarker(map, marker) {
    const latLngs = [ marker.getLatLng() ];
    const markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
  },


  setupController(controller, model, transition) {
    this._super(...arguments);
    const indexModel = this.controllerFor('button-net.index').get('model');
    if (this.controllerFor('button-net').get('model.privacy')) {
      if (get(this, 'session.isAuthenticated')) {
        if (isEmpty(get(this, 'session.currentUser.internalId'))) {
          get(this, 'session.currentUser').then((current) => {
            if (!get(this.controllerFor('button-net').get('model'), 'allowedUsers').includes(get(this, 'currentUser.internalId'))) {
              this.controllerFor('button-net').set('showAllowModal', true);
            }
          })
        } else {
          if (!get(this.controllerFor('button-net').get('model'), 'allowedUsers').includes(get(this, 'session.currentUser.internalId'))) {
            this.controllerFor('button-net').set('showAllowModal', true);
          }
        }
      } else {
        this.transitionTo('button-net.register')
      }
    }
    const meta = indexModel.buttons.meta;
    let transitionType = get(controller, 'transitionType');
    let bounds;
    // Check if stored transition type for setting received bounds
    // from server inside the meta response
    if (isEmpty(this.controller.get('allowMoveMap')) || this.controller.get('allowMoveMap')) {
      if (isEmpty(transition.router.oldState.params['button-net'])) {
        let leftCorner = L.latLng(this.controllerFor('button-net').get('model.latitude') + 1,
          this.controllerFor('button-net').get('model.longitude') - 1);
        let rightCorner = L.latLng(this.controllerFor('button-net').get('model.latitude') - 1,
          this.controllerFor('button-net').get('model.longitude') + 1);
        bounds = L.latLngBounds(leftCorner, rightCorner);
        transitionType = 'pointType';
        setProperties(controller, {
          bounds,
          disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
        });
        const geocodingAPI = 'https://maps.googleapis.com/maps/api/geocode/json?address=' +
          this.modelFor('button-net').locationName + '&key=AIzaSyAmLsgbCkKFf-vuMcV0oy5MlNzUtT1V8qM';
        return $.getJSON(geocodingAPI).then((json) => {
          if (json.status === 'OK') {
            leftCorner = L.latLng(json.results[0].geometry.bounds.northeast.lat,
              json.results[0].geometry.bounds.northeast.lng);
            rightCorner = L.latLng(json.results[0].geometry.bounds.southwest.lat,
              json.results[0].geometry.bounds.southwest.lng);
            bounds = L.latLngBounds(leftCorner, rightCorner);
          }
          transitionType = 'addressType';
          setProperties(controller, {
            bounds,
            disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
          });
        });
      } else {
        if(isEmpty(transitionType) || transitionType === 'pointType' || transitionType === 'addressType') {
          if (isEmpty(meta.swl) && !isEmpty(meta.lat)) {
            const leftCorner = L.latLng(meta.lat + 1, meta.lng - 1);
            const rightCorner = L.latLng(meta.lat - 1, meta.lng + 1);
            bounds = L.latLngBounds(leftCorner, rightCorner);
            setProperties(controller, {
              bounds,
              disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
            });
          } else {
            const leftCorner = L.latLng(meta.swl, meta.swlng);
            const rightCorner = L.latLng(meta.nel, meta.nelng);
            bounds = L.latLngBounds(leftCorner, rightCorner);
            setProperties(controller, {
              bounds,
              disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
            });
          }
        } else {
          const leftCorner = L.latLng(meta.swl, meta.swlng);
          const rightCorner = L.latLng(meta.nel, meta.nelng);
          bounds = L.latLngBounds(leftCorner, rightCorner);
          setProperties(controller, {
            bounds,
            disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
          });
        }
      }
      set(this.controllerFor('index'), 'reduceZoomNeed', false);
      if(!isEmpty(transition.queryParams.hideWelcome)) {
        set(this, 'hideWelcome', transition.queryParams.hideWelcome);
      }
      if(isEmpty(transition.queryParams.nel) && isEmpty(bounds)) {
        const leftCorner = L.latLng(meta.swl, meta.swlng);
        const rightCorner = L.latLng(meta.nel, meta.nelng);
        bounds = L.latLngBounds(leftCorner, rightCorner);
        setProperties(controller, {
          bounds,
          disableQueryParamsSetting: transitionType === 'pointType' || transitionType === 'addressType',
        });
      }
    }
  },
});
