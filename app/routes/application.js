import Route from '@ember/routing/route';
import { inject as service } from '@ember/service'
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import { reject } from 'rsvp';

export default Route.extend(ApplicationRouteMixin, {
  intl: service(),
  beforeModel() {
    this._super(...arguments);
    this.intl.setLocale(['es-es']);
    return function() {
      if (!localStorage.token) {
        return reject();
      }

      const adapter = this.container.lookup('adapter:application');
      adapter.set('headers', { 'Authorization': localStorage.token });
      return this.get('store').find('user', 'me')
        .then(function(user) {
          return {
            currentUser: user,
          };
        });
    };
  },
  setupController(controller, model) {
    this._super(controller, model);
    $('#loader').hide();
  },
});
