import { helper as buildHelper } from '@ember/component/helper';

export function headTitle([title, name]) {

  Ember.$('head title').text(title + name);

  // return title;
}

export default buildHelper(headTitle);
