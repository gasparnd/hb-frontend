import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({
  button: null,

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),

  actions: {
    nextStep() {
      this.transitionToRoute('buttons.new.location');
    },
  },
});
