import Controller from '@ember/controller';
import { readOnly, equal } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
  routing: service('-routing'),

  button: readOnly('model'),

  activeButtonRouteClass: equal('routing.currentRouteName', 'index.index.new-button.activate-button'),

  shareButtonRouteClass: equal('routing.currentRouteName', 'index.index.new-button.share'),

  lastRoute: 'type',
  showSettings: false,

  modalClass: computed(
    'activeButtonRouteClass',
    'shareButtonRouteClass',
    function() {
      if (this.activeButtonRouteClass) {
        return 'index-new-button__modal-activate-btn';
      } else if (this.shareButtonRouteClass) {
        return 'index-new-button__modal-close index-new-button__modal-container-class--bg-primary';
      }
      return 'index-new-button__modal-close';
    }
  ),

  actions: {
    closeAction() {
      this.transitionToRoute('index.index');
    },
  },
});
