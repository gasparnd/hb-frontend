import Controller from '@ember/controller';

export default Controller.extend({
  queryParams: [
    'email',
  ],

  email:null,

  actions: {
    closeAction() {
      this.transitionToRoute('index.index');
    },
  }
});
