import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';
import { isEmpty } from '@ember/utils';


export default Controller.extend({
  button: null,

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),


  actions: {
    nextStep() {
      this.transitionToRoute('button-net.profile.new-button.description');
    },
    closeAction() {
      this.transitionToRoute('button-net.profile');
    },
  },
});
