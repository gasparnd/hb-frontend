import Controller, { inject as controller } from '@ember/controller';
import { alias } from '@ember/object/computed';
import ActivateStepMixin from '../../../../mixins/activate-step-mixin';
import { set, get, computed } from '@ember/object';

export default Controller.extend(ActivateStepMixin,{
  button: alias('model'),

  buttonNetController: controller('button-net'),

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),
  loading: false,

  actions: {
    closeAction() {
      this.transitionToRoute('button-net.profile');
    },
  },

  activationRoute: 'button-net.profile.new-button.share',

});
