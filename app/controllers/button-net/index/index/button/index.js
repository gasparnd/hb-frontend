import Controller, { inject as controller } from '@ember/controller';
import { readOnly } from '@ember/object/computed';
import { set, get } from '@ember/object'
import { isEmpty } from '@ember/utils'
import { inject as service } from '@ember/service';

export default Controller.extend({
  routing: service('-routing'),
  button: readOnly('model.button'),
  chats: readOnly('model.chats'),
  buttonController: controller('button-net.index.index.button'),
  accessDenied: readOnly('buttonController.accessDenied'),

  selectedButton: null,
  socialShareNetwork: null,
  socialShareURL: null,

  actions: {
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
    openConversation() {
      if (get(this, 'button.creator.id') === get(this, 'currentUser.id') && get(this, 'currentUser.id') !== undefined) {
        this.transitionToRoute('button-net.index.index.button.my-chat', { queryParams: {id: this.button.id, isButton: true }});
      } else {
        this.transitionToRoute('button-net.index.index.button.chat', { queryParams: {id: this.button.id, isButton: true }});
      }
    },
    socialShareTransition() {
      switch (this.socialShareNetwork) {
        case 'facebook':
          window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
          break;
        case 'twitter':
          window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
          break;
        case 'linkedin':
          window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
          break;
        case 'whatsapp':
          window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
          break;
        default:
      }
    },
    removeButtonTransition() {
      set(this, 'selectedButton', null);
      this.transitionToRoute('button-net.index');
    },
  },
});
