import Controller, { inject as controller } from '@ember/controller';
import { inject as service } from '@ember/service';
import { or, readOnly } from '@ember/object/computed';
import { isEmpty } from '@ember/utils'
import { resolve } from 'rsvp';

export default Controller.extend({
  mediaQueries: service(),
  isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),
  buttonNetController: controller('button-net'),
  buttonNet: readOnly('buttonNetController.model'),

  email: null,
  model: null,

  actions: {
    backAction() {
      window.history.back();
    },
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
    searchByAddress(address, tag) {
      const urlParams = new URLSearchParams(window.location.search);
      let newTags = "";
      if (!isEmpty(urlParams.get('searchTags'))) {
        newTags = 'searchTags=' + urlParams.get('searchTags') + '&';
      }
      if (!isEmpty(tag)) {
        newTags += tag;
      }

      const queryParams = {
        latitude: null,
        longitude: null,
        address,
        northEastLat: null,
        northEastLng: null,
        southWestLat: null,
        southWestLng: null,
        userMarker: null,
        searchTags: newTags,
      };
      return resolve().then(() => {
        this.transitionToRoute('button-net.index', { queryParams });
      });
    },
  },
});
