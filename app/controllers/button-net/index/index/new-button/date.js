import Controller from '@ember/controller';
import { set, get, computed } from '@ember/object';


export default Controller.extend({

  isNew: computed('model', function() {
    return get(this, 'model.isNew');
  }),
  actions: {
    closeAction() {
      this.transitionToRoute('button-net.index.index');
    },
    nextStep(){
      this.transitionToRoute("button-net.index.index.new-button.activate-button");
    }
  },
});
