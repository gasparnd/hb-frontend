import Controller from '@ember/controller';
import { get, set, computed } from '@ember/object';
import { readOnly, or } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import ButtonsFiltersMixin from '../../mixins/buttons-filters-mixin';
import { isEmpty } from '@ember/utils';
import { resolve } from 'rsvp';
import { alias } from '@ember/object/computed';
import { htmlSafe } from '@ember/string';

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  cssClasses: 'notifications-index',
};

export default Controller.extend(
  ButtonsFiltersMixin,
  {
    notifications: service('notification-messages'),
    mediaQueries: service(),
    routing: service('-routing'),

    // QUERY PARAMS
    queryParams: [
      'searchTags',
    ],

    resetFilters: false,
    socialShareNetwork: null,
    socialShareURL: null,
    buttonNet: null,

    session: service('session'),
    currentUser: readOnly('session.currentUser'),
    avatar: readOnly('currentUser.avatar'),

    showProfile: false,

    showPulsedButtons: false,
    buttons: computed(
      'showPulsedButtons',
      'currentUser',
      'currentUser.{buttons.[],ownedButtons.[]}',
      {
        get() {
          const relationshipName = this.showPulsedButtons ? 'buttons' : 'ownedButtons';
          const currentUser = this.currentUser;

          get(currentUser, relationshipName)
            .then((buttons) => {
              set(this, 'buttons', buttons);
            });
        },
        set(key, value) { return value; },
      }
    ),

    isSmallDevice: or('mediaQueries.isMobile', 'mediaQueries.isTablet'),

    selectedButton: null,
    hasSelectedButton: computed('selectedButton', function() {
      if (!isEmpty(this.selectedButton)) {
        document.body.className = 'ember-application overflow-hidden';
        return true;
      }
      document.body.className = 'ember-application';
      return false;
    }),

    chats: computed(
      'selectedButton',
      {
        get() {
          this.selectedButton.chats.then((chats) => {
            set(this, 'chats', chats);
          });
        },
        set(key, value) {
          return value;
        },
      }
    ),

    isModalOpened: computed('routing.currentRouteName', function() {
      switch (this.routing.currentRouteName) {
        case 'index.index.button.index':
          document.body.className = 'ember-application overflow-hidden';
          return true;
        case 'index.index.button.chat':
          document.body.className = 'ember-application overflow-hidden';
          return true;
        default:
          if (this.routing.currentRouteName.includes('index.index.new-button')) {
            document.body.className = 'ember-application overflow-hidden';
            return true;
          }
          document.body.className = 'ember-application';
          return false;
      }
    }),

    //add top padding to __content
    asideBodyStyles: computed(
      'selectedTags.[]',
      'isSmallDevice',
      'buttonsDisplay',
      function() {
        get(this, 'tagsToApply');
        const issmall = get(this, 'isSmallDevice');
        const headerHeight = document.getElementsByClassName('component-header')[0].clientHeight;
        const searchTopBarHeight = document.getElementsByClassName('search-topbar')[0].clientHeight;
        var filtersHeight = 80;
        var pulsedCreatedHeight = 0;
        var bodyMinHeight = 0;
        if(!this.isSmallDevice)
        {filtersHeight =  document.getElementById('filters').clientHeight;
         pulsedCreatedHeight = document.getElementById('pulsedCreated').clientHeight;
         bodyMinHeight=1500;
        }
        const headerSectionHeight = headerHeight + searchTopBarHeight + filtersHeight + pulsedCreatedHeight ;
        const asideStyles = `padding-top: ${headerSectionHeight-60}px; min-height: ${bodyMinHeight}px`;
        return htmlSafe(asideStyles);
      }
    ),


    actions: {
      searchByAddress(address, tag) {
        const urlParams = new URLSearchParams(window.location.search);
        let newTags = "";
        if (!isEmpty(urlParams.get('searchTags'))) {
          newTags = 'searchTags=' + urlParams.get('searchTags') + '&';
        }
        if (!isEmpty(tag)) {
          newTags += tag;
        }


        const queryParams = {
          latitude: null,
          longitude: null,
          address,
          northEastLat: null,
          northEastLng: null,
          southWestLat: null,
          southWestLng: null,
          userMarker: null,
          searchTags: newTags,
        };
        return resolve().then(() => {
          this.transitionToRoute('index', { queryParams });
        });
      },

      setPulsedButtonsFilter(value) {
        set(this, 'showPulsedButtons', value);
      },

      selectButton(selectedButton) {
        set(this, 'selectedButton', selectedButton);
      },
      closeAction() {
        set(this, 'selectedButton', null);
        set(this, 'showProfile', null);
      },
      goHome() {
        this.send('closeAction');
        this.transitionToRoute('profile.index');
      },
      openConversation() {
        this.transitionToRoute('button-net.index.index.button.chat',
          { queryParams: { id: this.selectedButton.id, isButton: true }}
        );
      },
      socialShareTransition() {
        switch (this.socialShareNetwork) {
          case 'facebook':
            window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.socialShareURL, '_blank');
            break;
          case 'twitter':
            window.open('https://twitter.com/share?url=' + this.socialShareURL, '_blank');
            break;
          case 'linkedin':
            window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.socialShareURL, '_blank');
            break;
          case 'whatsapp':
            window.open('whatsapp://send?text=' + this.socialShareURL, '_blank');
            break;
          default:
        }
      },
      toggleProfileState() {
        this.toggleProperty('currentUser.active');
        return this.currentUser.save().then((user) => {
          if (user.active) {
            this.notifications.success(
              'Has activado tu usuario',
              NOTIFICATION_OPTIONS,
            );
          } else {
            this.notifications.info(
              'Has desactivado tu usuario. Tus botones no serán visibles para el resto de usuarios.',
              NOTIFICATION_OPTIONS,
            );
          }
        });
      },
    },
  }
);
