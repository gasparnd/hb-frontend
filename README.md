# Helpbuttons.org / Botonesdeayuda.org

Helpbuttons is a free and open-source network creation software written in Ember and Ruby on Rails. It serves as the platform for Helpbuttons.org and other projects. This version is the first version opened to the world.

Helpbuttons.org needs to become a big community to survive, if you like the idea of having a big open and free platform dedicated to collaboration please send a message to <help@helpbuttons.org>.

Also, if you want this project to survive please donate : https://helpbuttons.org/others?section=donate or subscribe here https://www.teaming.net/helpbuttons-org

* Interested in helping out?
** <help@helpbuttons.org>

Helpbuttons is the result of global collaboration and cooperation. The CREDITS
file lists technical contributors to the project. The COPYING file explains
Helpbuttons's copyright and license (GNU General Public License, version 3 or
later). Many thanks to the Helpbuttons community for testing and suggestions.


This is just the frontend app, remember that you need to download the backend too.

All documentation for Helpbuttons is available online
at http://www.helpbuttons.org/others

Helpbuttons.org - 2013-2021 (c) Angel Vazquez <angel@watchoutfreedom.org>
                         and the Helpbuttons.org Community
See COPYING and file headers for license info

Helpbuttons installation assistance
Angel Vazquez <angel@helpbuttons.org>/<angel@watchoutfreedom.org>
Carlos Silveira <carlos@helpbuttons.org>


## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://ember-cli.com/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* `git clone <repository-url>` this repository
* `cd my-app`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
* Visit your tests at [http://localhost:4200/tests](http://localhost:4200/tests).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Linting

* `npm run lint:js`
* `npm run lint:js -- --fix`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](https://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

### Instructions

API

```
rails s
```

EMBER

```
bower instal
ember s --proxy=http://localhost:3000
```
