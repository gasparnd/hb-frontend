import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  buttons: hasMany('button'),
  login: hasMany('login'),
});
