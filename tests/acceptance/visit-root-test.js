import { currentURL, visit } from '@ember/test-helpers';
import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import {
  authenticateSession,
  invalidateSession,
} from 'help-button/tests/helpers/ember-simple-auth';

module('Acceptance | visit root', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting / authenticated redirect to /dashboard', async function(assert) {
    authenticateSession(this.application, { token: 'something' });
    await visit('/');

    assert.equal(currentURL(), '/dashboard');
  });

  test('visiting /dashboard unauthenticated redirects to /login', async function(assert) {
    invalidateSession(this.application);
    await visit('/dashboard');

    assert.equal(currentURL(), '/login');
  });
});
