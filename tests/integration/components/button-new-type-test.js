import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | button new type', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    await render(hbs`{{button-new-type}}`);

    assert.dom('.component-button-new-type').exists({ count: 1 });

    // Template block usage:
    await render(hbs`
      {{#button-new-type}}
        template block text
      {{/button-new-type}}
    `);

    assert.dom('.component-button-new-type').exists({ count: 1 });
  });
});
