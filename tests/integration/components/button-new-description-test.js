import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | button new description', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    await render(hbs`{{button-new-description}}`);

    assert.dom('.component-button-new-description').exists({ count: 1 });

    // Template block usage:
    await render(hbs`
      {{#button-new-description}}
        template block text
      {{/button-new-description}}
    `);

    assert.dom('.component-button-new-description').exists({ count: 1 });
  });
});
