import EmberObject from '@ember/object';
import NewButtonOnAuthMixinMixin from 'help-button/mixins/new-button-on-auth-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | new-button-on-auth-mixin', function() {
  // Replace this with your real tests.
  test('it works', function(assert) {
    const NewButtonOnAuthMixinObject = EmberObject.extend(NewButtonOnAuthMixinMixin);
    const subject = NewButtonOnAuthMixinObject.create();
    assert.ok(subject);
  });
});
