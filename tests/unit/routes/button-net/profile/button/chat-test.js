import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | button-net/profile/button/chat', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:button-net/profile/button/chat');
    assert.ok(route);
  });
});
