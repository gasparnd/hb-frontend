import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | buttons/new/activate-button', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const route = this.owner.lookup('route:buttons/new/activate-button');
    assert.ok(route);
  });
});
