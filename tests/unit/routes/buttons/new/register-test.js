import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | buttons/new/register', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    const route = this.owner.lookup('route:buttons/new/register');
    assert.ok(route);
  });
});
